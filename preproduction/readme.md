# Herdlicka Blockout

It's a platforming level based on [surfing](https://www.youtube.com/watch?v=JAYmPea8Iso) and bunny [hopping](https://www.youtube.com/watch?v=4PZYPPlYd6A) (auto jump is enabled (hold space))

original layout:

![original_layout](E:\MYSTUFF\source\unity\gad-blockout\preproduction\original_layout.png)

final layout:

![final_layout](E:\MYSTUFF\source\unity\gad-blockout\preproduction\final_layout.png)

The original layout was changed due to laying a greater focus on surfing and time constraints.



pacing analysis:

![pacing-analysis](E:\MYSTUFF\source\unity\gad-blockout\preproduction\pacing-analysis.png)

pacing sections applied to layout:

 ![pacing_layout](E:\MYSTUFF\source\unity\gad-blockout\preproduction\pacing_layout.png)